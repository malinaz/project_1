function renderHome() {
    $("#content").empty();

    $("#content").append('<div class="title-box" id="title-box"></div>');
    $("#title-box").append('<h1>The Cookie Jar</h1>');
    $("#title-box").append('<p>Welcome to our online bakery!</p>');

    $("#content").append('<div class="contact-box" id="contact-box"></div>');
    $("#contact-box").append('<p>Find us on:</p>');
    $("#contact-box").append('<div class="contact" id="facebook-box"><i class="fab fa-facebook"></i></div>');
    $("#facebook-box").append('<p>Facebook</p>');

    $("#contact-box").append('<div class="contact" id="twitter-box"><i class="fab fa-twitter"></i></div>');
    $("#twitter-box").append('<p>Twitter</p>');

}

function main() {
    init();
    renderHome();
}

$(() => {
    main();
});
