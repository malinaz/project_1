const SERVER_URL = 'http://localhost:3000';

const user = {};

function buildHeader() {
	$("#container").append("<div class='header' id='header'>");

	$("#header").append("<img class='logo' id='logo' src='img/logo.jpg'>");

	$("#header").append("<h1 id='page-title'>");
	$("#page-title").append("Welcome!");
}

function renderLogInPage() {
	$("#container").append("<div class='login-container' id='login-container'>");

	$("#login-container").append("<input id='username-input' placeholder='User Name'>");
	$("#login-container").append("<input type='password' id='password-input' placeholder='Password'>");

	$("#login-container").append("<button id='login-button'>LOG IN</button>");
	$("#login-button").click( () => {
		const username = $("#username-input").val();
		const password = $("#password-input").val();

		if( username === '' || password === '') {
			renderPopUp("No empty fields allowed!");
		} else {
			performLogIn(username, password, (response) =>{
				if(response.length === 0){
					renderPopUp("Invalid username or password!");
				} else {
					window.location.href='../home/home.html';
				}
			})
		}
	})
}

function renderPopUp(message) {
	$("#container").append("<div class='popup-container' id='popup-container'>");

	$('#popup-container').append("<div class='overlay' id='overlay'>");
	$('#popup-container').append("<div class='popup' id='popup-success-donation'>");

	$("#popup-success-donation").append(`<p>${message}</p>`);
	$("#popup-success-donation").append("<button id='confirmation-button'>OK</button>");

	$("#confirmation-button").click( () => {
		$('#overlay').remove();
		$('#popup-container').remove();
	})
}

function performLogIn(username, password, callback) {
	$.ajax({
		url: `${SERVER_URL}/api/user/${username}/${password}`,
		type: 'GET',
		dataType: 'json',
		contentType: "application/json; charset=utf-8",
		success: function (response) {
			callback(response);
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function init() {
	buildHeader();
	renderLogInPage();
}

$( () => {
	init();
});
