const menuItems = [{
		name: 'HOME',
		id: 'home-btn'
	}, {
		name: 'PRODUCTS',
		id: 'products-btn'
	}, {
		name: 'CART',
		id: 'cart-btn'
	}];


function buildHeader() {
	$("#container").append("<div class='header' id='header'>");

	$("#header").append("<a id='home-container' href='../home/home.html'>HOME</a>");

	$("#header").append("<a id='products-container' href='../products/products.html'>PRODUCTS</a>");

	$("#header").append("<a id='cart-container' href='../cart/cart.html'>CART</a>");
}

function buildContent() {
	$("#container").append("<div class='content' id='content'>");
}

function init() {
	buildHeader();
	buildContent();
}

// export { init };

