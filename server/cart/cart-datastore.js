const Cart = require('./cart-model');

const cartDatastore = {

    createCart: (cart, success, fail) => {
        Cart.create(cart).then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    },

    // updateExistingProduct: (id, product, number, success, fail) => {
    //     Cart.findOneAndUpdate(
    //         {_id: id,
    //               products: product},
    //         {'$addToSet' : {
    //                 products: {
    //                     _id: product, number: 1
    //                 }
    //             }}).then((data) => {
    //         success(data);
    //     }).catch((error) => {
    //         fail(error);
    //     });
    // },

    addNewProduct: (id, product, success, fail) => {
        Cart.findOneAndUpdate(
            {_id: id},
            {'$addToSet' : {
                products: {
                    _id: product, number: 1
                }
                }}).then((data) => {
                    success(data);
        }).catch((error) => {
            fail(error);
        });
    },

    findByOwnerId: (id, success, fail) => {
        Cart.findOne({
            owner: id,
        }).then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    },

    insertProduct: (id, product, success, fail) => {
        Cart.findOneAndUpdate(
            {_id: id},
            {$push: {products: product}}
        ).then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    },

    removeCartById: (id, success, fail) => {
        Cart.deleteOne({
            userId: id
        }).then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    },
}

module.exports = cartDatastore;