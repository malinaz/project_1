const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cartSchema = new mongoose.Schema( {
    owner: {
        type: Schema.Types.ObjectID,
        ref: "user"
    },

    products: [{
        productId: Schema.Types.ObjectID,
        quantity: Number,
        name: String,
        price: Number,
        photo: String
    }]
});

const Cart = mongoose.model('cart', cartSchema);

module.exports = Cart;