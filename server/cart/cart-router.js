const express = require('express');
const cartDatastore = require('./cart-datastore');
const cartRouter = express.Router();

cartRouter.route('').post(createCart);
cartRouter.route('/:id').get(getCartByOwnerId);
cartRouter.route('/:id/:product').post(updateCartProducts);
cartRouter.route('/:id').delete(deleteCart);

cartRouter.route('/manage-products/:userId/:productId/:quantity/:name/:price/:photo').post(manageAddProduct);
cartRouter.route('/product/:userId/:productId').delete(removeProduct);

function removeProduct(request, response) {
    const userId = request.params.userId;
    const productId = request.params.productId;

    try {
        cartDatastore.findByOwnerId(userId, (cart) => {
            let itemIndex = -1;
            for(let i = 0; i < cart.products.length; i++) {
                if (cart.products[i].productId.equals(productId)) {
                    itemIndex = i;
                }
            }
            cart.products.splice(itemIndex, 1);
            cart = cart.save();
            return response.status(201).send(cart);
        })
    } catch (e) {
        console.log(error);
        response.status(500).send(error);
    }
}


function manageAddProduct(request, response) {
    const userId = request.params.userId;
    const productId = request.params.productId;
    const quantity = request.params.quantity;
    const name = request.params.name;
    const price = request.params.price;
    const photo = request.params.photo;

    try{
        cartDatastore.findByOwnerId(userId, (cart) => {
            if(cart) {
                let itemIndex = -1;
                for(let i = 0; i < cart.products.length; i++) {
                    if (cart.products[i].productId.equals(productId)) {
                        itemIndex = i;
                    }
                }

                if (itemIndex > -1) {
                    //product exists in the cart, update the quantity
                    let productItem = cart.products[itemIndex];
                    let q = productItem.quantity;
                    q++;
                    console.log(q);
                    productItem.quantity = quantity;
                    cart.products[itemIndex] = productItem;
                    cart = cart.save();
                    console.log(cart);
                    return response.status(201).send(cart);
                } else {
                    //product does not exists in cart, add new item
                    console.log("eu sunt nou");
                    cart.products.push({ productId, quantity,name, price, photo });
                    cart = cart.save();
                    return response.status(201).send(cart);
                }

            } else {
                let cart = {
                    owner: userId,
                    products: [{ productId, quantity, name, price, photo }]
                }
                cartDatastore.createCart(cart, (data) => {
                    console.log("cart created");
                    return response.status(201).send(data);
                }, (error) => {
                    console.log(error);
                });
            }

        }, (error) => {
            console.log(error);
        })
    } catch (error) {
        console.log(error);
        response.status(500).send(error);
    }
}

function createCart(request, response) {
    const cart = request.body;

    cartDatastore.createCart(cart, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

function getCartByOwnerId(request, response) {
    const id = request.params.id;

    cartDatastore.findByOwnerId(id, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

function updateCartProducts(request, response) {
    const id = request.params.id;
    const product = request.params.product;

    cartDatastore.insertProduct(id, product, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

function deleteCart(request, response) {
    const id = request.params.id;

    cartDatastore.removeCartById(id, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

module.exports = cartRouter;


