const Product = require('./product-model');

const productDatastore = {

    createProduct: (product, success, fail) => {
        Product.create(product).then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    },

    findById: (id, success, fail) => {
        Product.findOne({
            _id: id,
        }).then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    },

    findAll: ( success, fail) => {
        Product.find().then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    },

    findByCategory: (category, success, fail) => {
        Product.find({
            category: category,
        }).then((data) => {
            success(data);
        }).catch((error) => {
            fail(error);
        });
    }
}

module.exports = productDatastore;