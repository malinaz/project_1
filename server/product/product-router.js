const express = require('express');
const productDatastore = require('./product-datastore');
const productRouter = express.Router();

productRouter.route('').post(createProduct);
productRouter.route('/all').get(getAllProducts);
productRouter.route('/:id').get(getById);
productRouter.route('/category/:category').get(getByCategory);

function getAllProducts(request, response) {
    productDatastore.findAll( (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

function createProduct(request, response) {
    const product = request.body;

    productDatastore.createProduct(product, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

function getById(request, response) {
    const id = request.params.id;

    productDatastore.findById(id, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

function getByCategory(request, response) {
    const category = request.params.category;

    productDatastore.findByCategory(category, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

module.exports = productRouter;