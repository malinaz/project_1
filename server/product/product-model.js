const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new mongoose.Schema ({
    productName: String,
    productPrice: Number,
    productPhoto: String,
    description: String,
    category: String
});

const Product = mongoose.model('product', productSchema);

module.exports = Product;