const shared = {

	renderPopUp: (message) => {
		$("#container").append("<div class='popup-container' id='popup-container'>");

		$('#popup-container').append("<div class='overlay' id='overlay'>");
		$('#popup-container').append("<div class='popup' id='popup-success-donation'>");

		$("#popup-success-donation").append(`<p>${message}</p>`);
		$("#popup-success-donation").append("<button id='confirmation-button'>OK</button>");

		$("#confirmation-button").click( () => {
			$('#overlay').remove();
			$('#popup-container').remove();
		})
	},

	logoutPopUp: (message) =>
	{
		$("#container").append("<div class='popup-container' id='popup-container'>");

		$('#popup-container').append("<div class='overlay' id='overlay'>");
		$('#popup-container').append("<div class='popup' id='popup'>");

		$("#popup").append(`<p>${message}</p>`);
		$("#popup").append("<button id='confirmation-button'>YES</button>");
		$("#popup").append("<button id='negation-button'>NO</button>");

		$("#negation-button").click( () => {
			$('#overlay').remove();
			$('#popup-container').remove();
		})

		$("#confirmation-button").click( () => {
			$('#overlay').remove();
			$('#popup-container').remove();
			localStorage.removeItem('user');
			localStorage.removeItem('cart');
			window.location.href = '/login';
		})
	},

	buildHeader: () =>
	{
		$("#container").append("<div class='header' id='header'>");

		$("#header").append("<a id='home-container' href='/home'>HOME</a>");

		$("#header").append("<a id='products-container' href='/products'>PRODUCTS</a>");

		$("#header").append("<a id='cart-container' href='/cart'>CART</a>");

		$("#header").append("<a class ='logout' id='logout'>Log Out</a>");
		$("#logout").click( () => {
			shared.logoutPopUp("Are you sure you want to leave us? :(");
		})
	},

	buildContent: () => {
		$("#container").append("<div class='content' id='content'>");
	},

	init: () => {
		shared.buildHeader();
		shared.buildContent();
	}
};


