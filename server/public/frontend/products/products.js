const SERVER_URL = 'http://localhost:3000';
let user =  JSON.parse(localStorage.getItem('user'));
let cart =  JSON.parse(localStorage.getItem('cart'));
let productArray = [];
let cartArray = [];

function renderFilterBox() {
    $('#content').append("<div class='filter-box' id='filter-box'>");
    $('#filter-box').append("<button id='filter-all'><span>All<span></button>");
    $('#filter-all').click( () => {
        filterProducts("all");
    });

    $('#filter-box').append("<button id='filter-icecream'><span>Ice-Cream<span></button>");
    $('#filter-icecream').click( () => {
        filterProducts("ice-cream");
    });

    $('#filter-box').append("<button id='filter-cupcake'><span>Cupcakes<span></button>");
    $('#filter-cupcake').click( () => {
        filterProducts("cupcake");
    });

    $('#filter-box').append("<button id='filter-breakfast'><span>Sweet Breakfast<span></button>");
    $('#filter-breakfast').click( () => {
        filterProducts("breakfast");
    });
}

function filterProducts(filter) {
    if(filter === "all") {
        getAllProducts((response) => {
            renderProducts();
        });
    } else {
        getProductsByCategory(filter, (response) => {
            renderProducts();
        })
    }
}

function renderProducts() {
    $('#products-box').remove();

    $('#content').append("<div class='products-box' id='products-box'>");

    for(let i = 0; i < productArray.length; i++) {

        $('#products-box').append(`<div class="product-container" id="product${i}">`);

        $(`#product${i}`).append(`<img src="/frontend/products/products-img/${productArray[i].productPhoto}">`);
        $(`#product${i}`).append(`<h3>${productArray[i].productName}</h3>`);
        $(`#product${i}`).append(`<p>${productArray[i].description}</p>`);
        $(`#product${i}`).append(`<p>${productArray[i].productPrice}$</p>`);

        $(`#product${i}`).append(`<button id="add-product${i}"><i class="fas fa-plus"></i></button>`);
        $(`#add-product${i}`).click( () => {
            let quantity = 1;
            if(cartArray) {
                let x = newInCart(productArray[i]._id);
                if( x !== -1) {
                    quantity = cartArray[i].quantity + 1;
                }
            }
            addToCart(user._id, productArray[i]._id, quantity, productArray[i].productName, productArray[i].productPrice, productArray[i].productPhoto,(response) => {
                if(response) {
                    console.log(response);
                }
                console.log("hello");
                let x = newInCart(productArray[i]._id);
                if (x === -1) {
                    let product = {
                        productId: productArray[i]._id,
                        quantity: 1,
                        name: productArray[i].productName,
                        price: productArray[i].productPrice,
                        photo: productArray[i].productPhoto
                    }
                    cartArray.push(product);

                    } else {
                        cartArray[x].quantity++;
                    }
                    console.log(cartArray);
            })
        })
    }
}

function newInCart(id) {
    for(let i = 0; i < cartArray.length; i++) {
        if (cartArray[i].productId === id) {
            return i;
        }
    }
    return -1;
}


function getProductsByCategory(category, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/product/category/${category}`,
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            productArray = response;
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getAllProducts(callback) {
    $.ajax({
        url: `${SERVER_URL}/api/product/all`,
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            productArray = response;
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getProductById(id, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/product/${id}`,
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            productArray = response;
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function addToCart(userId, productId, quantity,name, price, photo, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/cart/manage-products/${userId}/${productId}/${quantity}/${name}/${price}/${photo}`,
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getUserCart(id, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/cart/${id}`,
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function main() {

    getUserCart(user._id, (result) => {
        if(result){
            cartArray = result.products;
        }
    })

    shared.init();
    renderFilterBox();
    getAllProducts( (response) => {
        renderProducts();
    });

}

$(() => {
    main();
});