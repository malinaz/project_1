const SERVER_URL = 'http://localhost:3000';
let user =  JSON.parse(localStorage.getItem('user'));

let cartArray = [];


function renderCartPage() {
    $('#cart-box').remove();

    $('#content').append("<div class='cart-box' id='cart-box'>");

    if (cartArray.length === 0) {
        $('#cart-box').append('<p>EMPTY CART!</p>');
    } else {

        $('#cart-box').append("<div class='cart-details' id='cart-details'>");
        $('#cart-details').append('<p>Your Cart</p>');

        $('#cart-box').append("<div class='cart-products' id='cart-products'>");
        for (let i = 0; i < cartArray.length; i++) {

            $('#cart-products').append(`<div class="cart-product-container" id="cart-product-container${i}">`);

            $(`#cart-product-container${i}`).append(`<img src="/frontend/products/products-img/${cartArray[i].photo}">`);
            $(`#cart-product-container${i}`).append(`<p class="cart-title">${cartArray[i].name}</p>`);
            $(`#cart-product-container${i}`).append(`<p class="cart-title">${cartArray[i].price}$</p>`);

            $(`#cart-product-container${i}`).append(`<div class="quantity-box" id="quantity-box${i}">`);

            $(`#quantity-box${i}`).append(`<button class="quantity-btn" id="minus-btn${i}">-</button>`);
            $(`#minus-btn${i}`).click(() => {
                let quantity = cartArray[i].quantity - 1;
                if (quantity == 0) {

                    deleteProductFromCart(user._id, cartArray[i].productId, (response) => {
                        cartArray.splice(i,1);
                        renderCartPage();
                    })

                } else {
                    addToCart(user._id, cartArray[i].productId, quantity, cartArray[i].name, cartArray[i].price, cartArray[i].photo, (response) => {
                        console.log(response);
                        cartArray[i].quantity--;
                        renderCartPage();
                    })
                }
            })


            $(`#quantity-box${i}`).append(`<p class="quantity">${cartArray[i].quantity}</p>`);

            $(`#quantity-box${i}`).append(`<button class="quantity-btn" id="plus-btn${i}">+</button>`);
            $(`#plus-btn${i}`).click(() => {
                let quantity = cartArray[i].quantity + 1;

                addToCart(user._id, cartArray[i].productId, quantity, cartArray[i].name, cartArray[i].price, cartArray[i].photo, (response) => {
                    console.log(response);
                    cartArray[i].quantity++;
                    renderCartPage();
                })

            })

        }

        $('#cart-box').append('<button class="send-order-btn" id="send-order-btn">Send Order</button>');
        $('#send-order-btn').click(() => {
            deleteCart(user._id, (response) => {
                cartArray = [];
                renderCartPage();
            })
        })
    }

}


function deleteCart(id, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/cart/${id}`,
        type: 'DELETE',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function deleteProductFromCart(userId, productId, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/cart/product/${userId}/${productId}`,
        type: 'DELETE',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getUserCart(id, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/cart/${id}`,
        type: 'GET',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function addToCart(userId, productId, quantity,name, price, photo, callback) {
    $.ajax({
        url: `${SERVER_URL}/api/cart/manage-products/${userId}/${productId}/${quantity}/${name}/${price}/${photo}`,
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function main() {
    shared.init();

    getUserCart(user._id, (response) => {
        if (response) {
            console.log("aiiici");
            cartArray = response.products;
        }
        renderCartPage();
        console.log("aiiicisasasas");
    })
}

$(() => {
    main();
});