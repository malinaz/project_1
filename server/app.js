const express = require('express');
const CONFIG = require('../server/config');
const mongoose = require('mongoose');

const userRouter = require('../server/user/user-router');
const productRouter = require('../server/product/product-router');
const cartRouter = require('../server/cart/cart-router');

const app = express();

function startServer() {
    app.listen(CONFIG.SERVER_PORT, function() {
        console.log(`Server running on port  ${CONFIG.SERVER_PORT}`);
    });
}

function startDatabase() {
    mongoose.connect(CONFIG.DATABASE, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    }).then(() => {
        console.log('Database connection: success!')
    })
}

function initRouters() {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "*");
        res.header("Access-Control-Allow-Methods", "*");
        next();
    });

    app.use(express.json());

    app.use(express.static(__dirname + '/public'));

    app.get('/login', function (request, response) {
        response.sendFile(__dirname + '/public/frontend/login/login.html');
    })

    app.get('/home', function (request, response) {
        response.sendFile(__dirname + '/public/frontend/home/home.html');
    })

    app.get('/products', function (request, response) {
        response.sendFile(__dirname + '/public/frontend/products/products.html');
    })

    app.get('/cart', function (request, response) {
        response.sendFile(__dirname + '/public/frontend/cart/cart.html');
    })

    app.use('/api/user', userRouter);
    app.use('/api/product', productRouter);
    app.use('/api/cart', cartRouter);
}

function init() {
    startServer();
    startDatabase();
    initRouters();
}
init();