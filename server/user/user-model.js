const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new mongoose.Schema( {
    name: String,
    username: String,
    password: String,
    userCart: {
        type: Schema.Types.ObjectID,
        ref: "cart"
    }
});

const User = mongoose.model('user', userSchema);

module.exports = User;