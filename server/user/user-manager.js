const userDatastore = require('./user-datastore');
const cartDatastore = require('../cart/cart-datastore');

const userManager = {

    logInUser: (username, password, success, fail) => {
        userDatastore.findByUserNameAndPassword(username, password, (user) =>{
            if(user._id) {
                cartDatastore.findByOwnerId(user._id, (cart) => {
                    if(cart._id) {
                        user.userCart = cart;
                    }
                }, (error) => {
                    console.log(error);
                });
            }
        }, fail(error));
    }
}

module.exports = userManager;