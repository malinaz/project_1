const express = require('express');
const userDatastore = require('./user-datastore');
const userManager = require('./user-manager');

const userRouter = express.Router();



userRouter.route('').post(createUser);
userRouter.route('/:username/:password').get(getByUserNameAndPassword);

//userRouter.route('/:username/:password').get(logInUser);

function logInUser(request, response) {
    const username = request.params.username;
    const password = request.params.password;

    userManager.logInUser(username, password, (data) =>{
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}


function createUser(request, response) {
    const user = request.body;

    userDatastore.createUser(user, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

function getByUserNameAndPassword(request, response) {
    const userName = request.params.username;
    const password = request.params.password;

    userDatastore.findByUserNameAndPassword(userName, password, (data) => {
        response.status(200).json(data);
    }, (error) => {
        response.status(500).json(error);
    });
}

module.exports = userRouter;