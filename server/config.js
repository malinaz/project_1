const CONFIG = {
    SERVER_PORT: 3000,
    DATABASE: 'mongodb://localhost/malina-hw14'
};

module.exports = CONFIG;